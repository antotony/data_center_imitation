class Server:
    def __init__(self, max_cpu, max_ram):
        self.max_cpu = max_cpu
        self.max_ram = max_ram
        self.current_cpu = 0
        self.current_ram = 0
        self.vm_list = []

    def add_vm(self, vm):  # добавление ВМ
        if self.current_cpu + vm.cpu > self.max_cpu or self.current_ram + vm.ram > self.max_ram:
            return False
        self.vm_list += [vm]
        self.current_cpu += vm.cpu
        self.current_ram += vm.ram
        return True

    def remove_vm(self, vm):  # удаление ВМ
        self.vm_list.remove(vm)
        self.current_cpu -= vm.cpu
        self.current_ram -= vm.ram

    def get_cpu_percentage(self):  # вывод процента загруженности CPU сервера
        cpu_percent = self.current_cpu / self.max_cpu * 10000
        return float(str(int(cpu_percent / 100)) + "." + str(int(cpu_percent % 100)))

    def get_ram_percentage(self):  # вывод процента загруженности RAM сервера
        ram_percent = self.current_ram / self.max_ram * 10000
        return float(str(int(ram_percent / 100)) + "." + str(int(ram_percent % 100)))

    def score(self, mean_cpu, mean_ram):
        if self.current_cpu == 0:
            return 0
        return abs(self.get_cpu_percentage() - mean_cpu) + abs(self.get_ram_percentage() - mean_ram)
