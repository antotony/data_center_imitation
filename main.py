from virtual_machine import VirtualMachine
from server import Server
from data_center import DataCenter

import matplotlib.pyplot as plt
import pylab
from tkinter import *
from random import randint
from time import sleep

print("Введите количество серверов ")
server_count = int(input())
print("Введите количество часов работы серверов ")
total_time = int(input())
print("Введите стартовое количество виртуальных машин ")
start_vm_count = int(input())
print("Введите вероятность появления нового заказа в % ")
verpov = int(input())


# В этой функции рисуются графики
def draw_graphs(x, y_list, mean, work_mean):
    plt.figure(figsize=[10, 4.8])
    plt.subplot(121)
    plt.title("CPU% от времени")
    plt.ylabel("CPU%")
    plt.xlabel("Время (в часах)")
    for y in y_list:
        plt.plot(x, y["cpu"], "k")
    plt.plot(x, mean["cpu"], "r")
    plt.plot(x, work_mean["cpu"], "b")
    plt.subplot(122)
    plt.title("RAM% от времени")
    plt.ylabel("RAM%")
    plt.xlabel("Время (в часах)")
    for y in y_list:
        plt.plot(x, y["ram"], "k")
    plt.plot(x, mean["ram"], "r")
    plt.plot(x, work_mean["ram"], "b")
    plt.show()


def main(window, my_data_center, grid_servers, total_line):
    # Основной цикл
    x = []
    y_list = [{"cpu": [], "ram": []} for i in range(server_count)]
    mean = {"cpu": [], "ram": []}
    work_mean = {"cpu": [], "ram": []}
    score = 0
    for time in range(total_time):
        x += [time]
        score += my_data_center.score()
        if randint(0, 100) > 100 - verpov:  # добавление ВМ
            # my_data_center.lite_add_vm(VirtualMachine(generate=True))
            my_data_center.lite_add_vm_and_move_vm(VirtualMachine(generate=True))
            # my_data_center.shuffle_add_vm(VirtualMachine(generate=True))
            # my_data_center.less_mean_lite_add_vm(VirtualMachine(generate=True))
            # my_data_center.less_mean_shuffle_add_vm(VirtualMachine(generate=True))
        for i in range(server_count):  # добавление данных в таблицу
            grid_servers[i]["cpu"].configure(text=str(my_data_center.servers[i].current_cpu))
            grid_servers[i]["ram"].configure(text=str(my_data_center.servers[i].current_ram))
            grid_servers[i]["cpu_percentage"].configure(text=str(my_data_center.servers[i].get_cpu_percentage()) + "%")
            y_list[i]["cpu"] += [float(my_data_center.servers[i].get_cpu_percentage())]
            grid_servers[i]["ram_percentage"].configure(text=str(my_data_center.servers[i].get_ram_percentage()) + "%")
            y_list[i]["ram"] += [float(my_data_center.servers[i].get_ram_percentage())]

        used_cpu, used_ram = my_data_center.total_used()  # высчитывание и добавление использованных CPU и RAM
        total_line["used_cpu"].configure(text=str(used_cpu))
        total_line["used_ram"].configure(text=str(used_ram))
        total_line["cpu_percentage"].configure(text=str(my_data_center.mean_cpu()) + "%")
        mean["cpu"] += [my_data_center.mean_cpu()]
        work_mean["cpu"] += [my_data_center.mean_work_cpu()]
        total_line["ram_percentage"].configure(text=str(my_data_center.mean_ram()) + "%")
        mean["ram"] += [my_data_center.mean_ram()]
        work_mean["ram"] += [my_data_center.mean_work_ram()]

        grid_time.configure(text=str(time))

        my_data_center.tact()  # минус час из срока ВМ
        my_data_center.remove_ended_vm()  # удаление ВМ с закончившимся сроком
        window.update()  # обновление таблицы
    window.quit()
    score /= total_time
    score += 1000 * my_data_center.lost / (total_time / 24)
    file = open("result.txt", "w")
    counter = 1
    for server in my_data_center.servers:
        file.write("Виртуальная машина №" + str(counter) + ": CPU=" + str(
            float(server.get_cpu_percentage())) + "%; RAM=" + str(server.get_ram_percentage()) + "%\n")
        counter += 1
    file.write("Средние значения: CPU=" + str(float(my_data_center.mean_cpu())) + "%; RAM=" + str(
        my_data_center.mean_ram()) + "%\n")
    file.write("Потеряно заказов: " + str(my_data_center.lost))
    print("Потеряно заказов: " + str(my_data_center.lost))
    file.write("Оценка алгоритма: " + str(score))
    print("Оценка алгоритма: " + str(score))
    file.close()
    draw_graphs(x, y_list, mean, work_mean)


# инициаллизация дата-центра
my_data_center = DataCenter()
for i in range(server_count):
    new_server = Server(40, 40 * 8)
    my_data_center.add_server(new_server)
for i in range(start_vm_count):
    vm = VirtualMachine(generate=True)
    # my_data_center.lite_add_vm(vm)
    # my_data_center.shuffle_add_vm(vm)
    # my_data_center.less_mean_lite_add_vm(vm)
    my_data_center.less_mean_shuffle_add_vm(vm)

# оформление окна
window = Tk()
window.title("Мониторинг использования ресурсов серверов")
window.geometry('500x300')
# номер сервера
b = Label(window, text="Номер сервера", font=("Bold",))
b.grid(row=0, column=0)
for i in range(server_count):
    b = Label(window, text="#" + str(i + 1))
    b.grid(row=i + 1, column=0)
# количество занятых процессоров
b = Label(window, text="CPU", font=("Bold",))
b.grid(row=0, column=1)
# объем занятой оперативной памяти
b = Label(window, text="RAM", font=("Bold",))
b.grid(row=0, column=2)
# общее количество доступных процессоров
b = Label(window, text="MAX CPU", font=("Bold",))
b.grid(row=0, column=3)
for i in range(server_count):
    b = Label(window, text=str(my_data_center.servers[i].max_cpu))
    b.grid(row=i + 1, column=3)
# общий объем оперативной памяти
b = Label(window, text="MAX RAM", font=("Bold",))
b.grid(row=0, column=4)
for i in range(server_count):
    b = Label(window, text=str(my_data_center.servers[i].max_ram))
    b.grid(row=i + 1, column=4)
# процентное соотношение занятых процессоров к свободным
b = Label(window, text="CPU%", font=("Bold",))
b.grid(row=0, column=5)
# общий объем оперативной памяти
b = Label(window, text="RAM%", font=("Bold",))
b.grid(row=0, column=6)

b = Label(window, text="Текущее время:")
b.grid(row=server_count + 4, column=0)

grid_time = Label(window, text="0")
grid_time.grid(row=server_count + 4, column=1)

grid_servers = []
for i in range(server_count):
    grid_servers.append({})
    grid_servers[i]["cpu"] = Label(window, text=str(my_data_center.servers[i].current_cpu))
    grid_servers[i]["cpu"].grid(row=i + 1, column=1)
    grid_servers[i]["ram"] = Label(window, text=str(my_data_center.servers[i].current_ram))
    grid_servers[i]["ram"].grid(row=i + 1, column=2)
    grid_servers[i]["cpu_percentage"] = Label(window, text=str(my_data_center.servers[i].get_cpu_percentage()) + "%")
    grid_servers[i]["cpu_percentage"].grid(row=i + 1, column=5)
    grid_servers[i]["ram_percentage"] = Label(window, text=str(my_data_center.servers[i].get_ram_percentage()) + "%")
    grid_servers[i]["ram_percentage"].grid(row=i + 1, column=6)

# на последней строке выводятся общие параметры
b = Label(window, text="Общее")
b.grid(row=server_count + 1, column=0)
total_line = {}
total_line["used_cpu"] = Label(window, text=str(0))
total_line["used_cpu"].grid(row=server_count + 1, column=1)
total_line["used_ram"] = Label(window, text=str(0))
total_line["used_ram"].grid(row=server_count + 1, column=2)
b = Label(window, text=str(my_data_center.total_cpu))
b.grid(row=server_count + 1, column=3)
b = Label(window, text=str(my_data_center.total_ram))
b.grid(row=server_count + 1, column=4)
total_line["cpu_percentage"] = Label(window, text=str(0) + "%")
total_line["cpu_percentage"].grid(row=server_count + 1, column=5)
total_line["ram_percentage"] = Label(window, text=str(0) + "%")
total_line["ram_percentage"].grid(row=server_count + 1, column=6)
btn = Button(window)
main(window, my_data_center, grid_servers, total_line)
