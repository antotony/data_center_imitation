from random import shuffle


class DataCenter:
    def __init__(self):
        self.servers = []
        self.total_cpu = 0
        self.total_ram = 0
        self.lost = 0

    def add_server(self, server):  # добавление сервера
        self.servers += [server]
        self.total_cpu += server.max_cpu
        self.total_ram += server.max_ram

    def total_used(self):  # использованные CPU и RAM
        cpu = 0
        ram = 0
        for server in self.servers:
            cpu += server.current_cpu
            ram += server.current_ram
        return cpu, ram

    def mean_cpu(self):  # средний CPU
        used_cpu, _ = self.total_used()
        cpu_percent = used_cpu / self.total_cpu * 10000
        return float(str(int(cpu_percent / 100)) + "." + str(int(cpu_percent % 100)))

    def mean_work_cpu(self):  # средний CPU работающих серверов
        used_cpu = 0
        total_cpu = 0
        for server in self.servers:
            if not server.current_cpu == 0:
                used_cpu += server.current_cpu
                total_cpu += server.max_cpu
        if total_cpu != 0:
            cpu_percent = used_cpu / total_cpu * 10000
        else:
            cpu_percent = 0.0
        return float(str(int(cpu_percent / 100)) + "." + str(int(cpu_percent % 100)))

    def mean_ram(self):  # средний RAM
        _, used_ram = self.total_used()
        ram_percent = used_ram / self.total_ram * 10000
        return float(str(int(ram_percent / 100)) + "." + str(int(ram_percent % 100)))

    def mean_work_ram(self):  # средний RAM работающих серверов
        used_ram = 0
        total_ram = 0
        for server in self.servers:
            if not server.current_cpu == 0:
                used_ram += server.current_cpu
                total_ram += server.max_cpu
        if total_ram != 0:
            ram_percent = used_ram / total_ram * 10000
        else:
            ram_percent = 0.0
        return float(str(int(ram_percent / 100)) + "." + str(int(ram_percent % 100)))

    def lite_add_vm(self, vm):  # добавление ВМ по порядку серверов
        i = 0
        while not self.servers[i].add_vm(vm):
            i += 1
            if i == len(self.servers):
                self.lost += 1
                return 1
        return 0

    def lite_add_vm_and_move_vm(self, vm):  # добавление ВМ по порядку серверов
        i = 0
        while not self.servers[i].add_vm(vm):
            i += 1
            if i == len(self.servers):
                self.lost += 1
                return 1
        i = 0
        j = len(self.servers) - 1
        while self.servers[j].current_cpu == 0:
            j -= 1
        while j > 1:
            for vm in self.servers[j].vm_list:
                while i != j and not self.servers[i].add_vm(vm):
                    i += 1
                if i != j:
                    self.servers[j].remove_vm(vm)
                i = 0
            j -= 1
        return 0

    def shuffle_add_vm(self, vm):  # добавление ВМ по смешанному порядку серверов (рандом)
        i = 0
        servers_list = self.servers
        shuffle(servers_list)
        while not servers_list[i].add_vm(vm):
            i += 1
            if i == len(self.servers):
                self.lost += 1
                return 1
        return 0

    def less_mean_lite_add_vm(self, vm):  # добавление ВМ к серверу с заполненностью меньше средней заполненности всех серверов, иначе по списку
        servers_list = self.servers
        for server in servers_list:
            if server.current_cpu < self.mean_cpu() and server.current_ram < self.mean_ram():
                if server.add_vm(vm):
                    return 0
        for server in servers_list:
            if not (server.current_cpu > self.mean_cpu() and server.current_ram > self.mean_ram()):
                if server.add_vm(vm):
                    return 0
        return self.lite_add_vm(vm)  # иначе добавление ВМ к серверу по списку

    def less_mean_shuffle_add_vm(self, vm):  # добавление ВМ к серверу с заполненностью меньше средней заполненности всех серверов, иначе рандом
        servers_list = self.servers
        shuffle(servers_list)
        for server in servers_list:
            if server.current_cpu < self.mean_cpu() and server.current_ram < self.mean_ram():
                if server.add_vm(vm):
                    return 0
        for server in servers_list:
            if not (server.current_cpu > self.mean_cpu() and server.current_ram > self.mean_ram()):
                if server.add_vm(vm):
                    return 0
        return self.shuffle_add_vm(vm)  # иначе добавление ВМ к рандромному серверу

    def tact(self):  # такт времени, уменьшается продолжительность жизни ВМ
        for server in self.servers:
            for vm in server.vm_list:
                vm.life_time -= 1

    def remove_ended_vm(self):  # удаление просрочившейся ВМ
        for server in self.servers:
            for vm in server.vm_list:
                if vm.life_time <= 0:
                    server.remove_vm(vm)

    def servers_used(self):
        result = 0
        for server in self.servers:
            if server.current_cpu > 0:
                result += 1
        return result

    def score(self):
        result = 0
        for server in self.servers:
            result += server.score(self.mean_work_cpu(), self.mean_work_ram())
        return result / len(self.servers) + 50 * self.servers_used()
