from random import randint


class VirtualMachine:
    def __init__(self, cpu=None, ram=None, life_time=None, generate=False):
        if generate: # генерация данных о ВМ
            cpu, ram, life_time = randint(1, 8), randint(8, 64), randint(24, 240)
            if randint(0, 100) > 90:
                cpu += randint(3, 10)
            if randint(0, 100) > 90:
                ram += randint(24, 80)
            if randint(0, 100) > 90:
                life_time += randint(24, 120)
            self.cpu = cpu
            self.ram = ram
            self.life_time = life_time
        else:
            self.cpu = cpu
            self.ram = ram
            self.life_time = life_time
